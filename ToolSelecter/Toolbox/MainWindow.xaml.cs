﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Jiuyong.Models;

namespace Jiuyong
{
	/// <summary>
	/// MainWindow.xaml 的交互逻辑
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
#if DEBUG
			//ContriverButton.Click += ContriverButton_Click;
#else
			ContriverPanel.Visibility = Visibility.Collapsed;
#endif
			this.Loaded += MainWindow_Loaded;

		}

		private void ContriverButton_Click(object sender, RoutedEventArgs e)
		{
			//Register(sender, e);
			//Events.ShowMessage("新消息");
		}

		public MainWindowViewModel VM => DataContext as MainWindowViewModel;

		void MainWindow_Loaded(object sender, RoutedEventArgs e)
		{
			//CheckSingleton();
			VM?.LoadToolsConfig();
			AddCommands();
			RegisterEvents();
		}

		private void RegisterEvents()
		{
			this.Subscribe(AggregatorEvents.Error,
				resultCallback: e =>
				{
					MessageBox.Show($"程序运行时发生了未知的异常，提示信息为：“{e.Message}”", $"{App.Current.MainWindow.Title}-出现错误");
				});
		}

		partial void AddCommands();

		private void Button_ContextMenuOpening(object sender, ContextMenuEventArgs e)
		{
			//const int EnableMenuItemIndex = 2;//按顺序查找不利于方便的调整菜单。
			const string EnableMenuItemName = "EnableMenuItem";
			var btn = sender as System.Windows.FrameworkElement;
			var dct = btn.DataContext as ViewModels.ToolViewModel;
			var mn = btn.ContextMenu;
			//var mi = this.FindResource(EnableMenuItemName) as MenuItem;//也没戏。
			var m = mn.Items.Cast<MenuItem>().Where(x => x.Name == EnableMenuItemName).Single();
			//var m = mn.Items[EnableMenuItemIndex] as MenuItem;
			m.Header = dct.IsEnable ? "禁用" : "启用";
			//m.IsEnabled = btn.IsEnabled;
		}

	}


}
