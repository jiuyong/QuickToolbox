﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace Jiuyong
{
	/// <summary>
	/// App.xaml 的交互逻辑
	/// </summary>
	public partial class App : Application
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			if (ApplicationSingleton.IsFirstInstance)
			{
				base.OnStartup(e);
				ApplicationSingleton.StartWatch(() =>
				{
					this.Dispatcher.Invoke(new Action(() =>
					{
						var window = this.MainWindow;
						if (null == window)
						{
							AggregatorEvents.Log("收到其它实例发送的单实例启动信号，但是本实例缺少主窗口，而没能进一步处理。");
						}
						else
						{
							window.Show();
							window.Activate();
							if (WindowState.Minimized == window.WindowState)
							{
								window.WindowState = WindowState.Normal;
							}
							Events.ShowMessage($"本程序“{ApplicationSingleton.Name}”已经在运行中。");
						}
					}));
				});
			}
			else
			{
				//这时候主窗体还没加载，因此消息的事件订阅处理不会执行。
				//MessageBox.Show("本程序已经在运行中。");
				ApplicationSingleton.ActivateOtherInstance();//2
				this.Shutdown(1);
			}
		}
	}
}
