﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Jiuyong.ViewModels
{

	/// <remarks>不能实例化的抽象类会导致 Blend 设计器出错。</remarks>
	public partial class ToolViewModel : INotifyPropertyChanged, INotifyPropertyChanging
	{
		#region INotifyPropertyChanged 成员

		/// <summary>
		/// 当属性更改后发生。
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// 发生属性更改后，通知订阅者。
		/// </summary>
		/// <param name="propertyName">更改的属性名称。</param>
		protected void OnPropertyChanged(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion

		#region  INotifyPropertyChanging 成员

		/// <summary>
		/// 将要更改属性值时发生。
		/// </summary>
		public event PropertyChangingEventHandler PropertyChanging;

		/// <summary>
		/// 通知客户端某个属性值将更改。
		/// </summary>
		/// <param name="propertyName">即将更改的属性名称。</param>
		protected virtual void OnPropertyChanging(string propertyName)
		{
			PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
		}

		#endregion

		#region 数据模型

		#region Description

		private string _description;

		public string Description
		{
			get
			{
				return _description;
			}

			set
			{
				OnPropertyChanging(nameof(Description));
				_description = value;
				OnPropertyChanged(nameof(Description));
			}
		}
		#endregion

		#region Title - 显示文本

		/// <summary>
		/// 显示文本
		/// </summary>
		private string _title;

		/// <summary>
		/// 显示文本
		/// </summary>
		public string Title
		{
			get
			{
				return _title;
			}

			set
			{
				OnPropertyChanging(nameof(Title));
				_title = value;
				OnPropertyChanged(nameof(Title));
			}
		}
		#endregion

		#region Name

		private string _name;

		public string Name
		{
			get
			{
				return _name;
			}

			set
			{
				OnPropertyChanging(nameof(Name));
				_name = value;
				OnPropertyChanged(nameof(Name));
			}
		}
		#endregion

		#region FullPath

		private string _fullPath;

		public string FullPath
		{
			get
			{
				return _fullPath;
			}

			set
			{
				OnPropertyChanging(nameof(FullPath));
				_fullPath = value;
				OnPropertyChanged(nameof(FullPath));
			}
		}
		#endregion

		#region Args

		private string _args;

		public string Args
		{
			get
			{
				return _args;
			}

			set
			{
				OnPropertyChanging(nameof(Args));
				_args = value;
				OnPropertyChanged(nameof(Args));
			}
		}
		#endregion

		#region RegisterDateTime

		private DateTime _registerDateTime;

		public DateTime RegisterDateTime
		{
			get
			{
				return _registerDateTime;
			}

			set
			{
				OnPropertyChanging(nameof(RegisterDateTime));
				_registerDateTime = value;
				OnPropertyChanged(nameof(RegisterDateTime));
			}
		}
		#endregion

		#region ModifDateTime

		private DateTime _modifDateTime;

		public DateTime ModifDateTime
		{
			get
			{
				return _modifDateTime;
			}

			set
			{
				OnPropertyChanging(nameof(ModifDateTime));
				_modifDateTime = value;
				OnPropertyChanged(nameof(ModifDateTime));
			}
		}
		#endregion

		#region IsEnable - 是否可用

		/// <summary>
		/// 是否可用
		/// </summary>
		private bool _isEnable = true;

		/// <summary>
		/// 是否可用
		/// </summary>
		public bool IsEnable
		{
			get
			{
				return _isEnable;
			}

			set
			{
				OnPropertyChanging(nameof(IsEnable));
				_isEnable = value;
				OnPropertyChanged(nameof(IsEnable));
			}
		}
		#endregion

		#region WorkingDirectory

		private string _workingDirectory;

		public string WorkingDirectory
		{
			get
			{
				return _workingDirectory;
			}

			set
			{
				OnPropertyChanging(nameof(WorkingDirectory));
				_workingDirectory = value;
				OnPropertyChanged(nameof(WorkingDirectory));
			}
		}
		#endregion

		#region Icon

		private byte[] _icon;

		public byte[] Icon
		{
			get
			{
				return _icon;
			}

			set
			{
				OnPropertyChanging(nameof(Icon));
				_icon = value;
				OnPropertyChanged(nameof(Icon));
			}
		}
		#endregion

		#region RunAsAdministrator - 以管理员模式运行

		/// <summary>
		/// 以管理员模式运行
		/// </summary>
		private bool _runAsAdministrator;

		/// <summary>
		/// 以管理员模式运行
		/// </summary>
		public bool RunAsAdministrator
		{
			get
			{
				return _runAsAdministrator;
			}

			set
			{
				OnPropertyChanging(nameof(RunAsAdministrator));
				_runAsAdministrator = value;
				OnPropertyChanged(nameof(RunAsAdministrator));
			}
		}
		#endregion

		#endregion

		#region 模型转换
		//如果将泛型基类的模型转换功能实现为实例方法，
		//那样页无法做可扩展性的进行通用化处理设计，
		//因此将模型转换工作都实现为通过模板生成的类静态方法。

		///// <summary>
		///// 为了兼容设计工具必须实现无参构造函数。
		///// </summary>
		//public ToolViewModel()
		//{
		//}

		/// <summary>
		/// 使用“模型”数据来创建“视图模型”，以便直接在 LinQ 中能直接转换。
		/// </summary>
		/// <param name="model"></param>
		public static ToolViewModel CreateViewModel(Jiuyong.Models.Tool model)//:this()
		{
			var vm = new ToolViewModel();
			vm.InnerLoadModel(model);
			return vm;
		}

		/// <summary>
		/// 以“外部”方式加载 “模型（Jiuyong.Models.Tool）” 的数据，此种方式会引发属性更改通知事件。
		/// </summary>
		/// <param name="model">数据模型</param>
		public virtual void OuterLoadModel(Jiuyong.Models.Tool model)
		{
			this.Description = model.Description;
			this.Title = model.Title;
			this.Name = model.Name;
			this.FullPath = model.FullPath;
			this.Args = model.Args;
			this.RegisterDateTime = model.RegisterDateTime;
			this.ModifDateTime = model.ModifDateTime;
			this.IsEnable = model.IsEnable;
			this.WorkingDirectory = model.WorkingDirectory;
			this.Icon = model.Icon;
			this.RunAsAdministrator = model.RunAsAdministrator;
		}

		/// <summary>
		/// 加载 “模型（Jiuyong.Models.Tool）” 的数据,但是与“LoadOuterModel(Model)”方法 不同，这里不会触发绑定验证而导致异常。
		/// </summary>
		/// <param name="model">数据模型</param>
		public virtual void InnerLoadModel(Jiuyong.Models.Tool model)
		{
		this._description = model.Description;
		this._title = model.Title;
		this._name = model.Name;
		this._fullPath = model.FullPath;
		this._args = model.Args;
		this._registerDateTime = model.RegisterDateTime;
		this._modifDateTime = model.ModifDateTime;
		this._isEnable = model.IsEnable;
		this._workingDirectory = model.WorkingDirectory;
		this._icon = model.Icon;
		this._runAsAdministrator = model.RunAsAdministrator;
		}

		/// <summary>
		/// 导出“视图模型（ToolViewModel）” 的数据到 “模型（Jiuyong.Models.Tool）” 中。
		/// </summary>
		/// <returns>数据模型</returns>
		public virtual Jiuyong.Models.Tool ToModel()
		{
			var model = new Jiuyong.Models.Tool();

		model.Description = this.Description;
		model.Title = this.Title;
		model.Name = this.Name;
		model.FullPath = this.FullPath;
		model.Args = this.Args;
		model.RegisterDateTime = this.RegisterDateTime;
		model.ModifDateTime = this.ModifDateTime;
		model.IsEnable = this.IsEnable;
		model.WorkingDirectory = this.WorkingDirectory;
		model.Icon = this.Icon;
		model.RunAsAdministrator = this.RunAsAdministrator;

			return model;
		}

		#endregion

	}
}