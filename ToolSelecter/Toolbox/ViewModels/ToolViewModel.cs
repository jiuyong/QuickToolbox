﻿using Jiuyong.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Jiuyong.ViewModels
{
	public partial class ToolViewModel //: ViewModelBase<Tool>
	{

#if V0

		#region Model

		Tool _tool;
		public ToolViewModel()
		{
			_tool = new Tool();
		}
		public ToolViewModel(Tool model)
		{
			InnerLoadModel(model);
		}
		public override void InnerLoadModel(Tool model)
		{
			_tool = model;
		}

		public override void OuterLoadModel(Tool model)
		{
			_tool = model;
		}

		public override Tool ToModel()
		{
			return _tool;
		}

		#endregion

		#region 显示文本

		/// <summary>
		/// 显示文本。
		/// </summary>
		//[System.ComponentModel.DataAnnotations.Display(Name = "显示文本", Description = "显示文本。", AutoGenerateField = true)]
		//[System.ComponentModel.DataAnnotations.Required(ErrorMessage="“显示文本”不能为空！")]
		[System.ComponentModel.Category("模型")]  //可用于属性面板中的分组。
		[System.ComponentModel.Description("显示文本。")]
		[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]//在编辑面板（例如 Blend ）中如何显示。
																									//[System.ComponentModel.Browsable(false)]	//在属性面板（例如 VisualStudio）中隐藏。
		public string Title
		{
			get { return _tool.Title; }
			set
			{
				ValidateProperty("Title", value);
				_tool.Title = value;
				OnPropertyChanged("Title");
			}
		}

		#endregion

		#region 是否可用

		/// <summary>
		/// 是否可用。
		/// </summary>
		//[System.ComponentModel.DataAnnotations.Display(Name = "是否可用", Description = "是否可用。", AutoGenerateField = true)]
		//[System.ComponentModel.DataAnnotations.Required(ErrorMessage="“是否可用”不能为空！")]
		[System.ComponentModel.Category("模型")]  //可用于属性面板中的分组。
		[System.ComponentModel.Description("是否可用。")]
		[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]//在编辑面板（例如 Blend ）中如何显示。
																									//[System.ComponentModel.Browsable(false)]	//在属性面板（例如 VisualStudio）中隐藏。
		public bool IsEnable
		{
			get { return _tool.IsEnable; }
			set
			{
				ValidateProperty("IsEnable", value);
				_tool.IsEnable = value;
				OnPropertyChanged("IsEnable");
			}
		}

		#endregion

		public string Shell => _tool.Shell;

		public string WorkingDirectory => _tool.WorkingDirectory;

		public string Description => _tool.Description;

		public System.IO.FileInfo File { get; set; }
#endif

		//public string Shell => $"{FullPath} {Args}";

		public ImageSource IconImageSource
		{
			get
			{
				ImageSource r = null;
				var bs = this.Icon;
				if (null == bs)
				{
					//.
				}
				else
				{
					r = new PngBitmapDecoder(
						new System.IO.MemoryStream(
							this.Icon
							)
						, BitmapCreateOptions.PreservePixelFormat
						, BitmapCacheOption.Default
						).Frames.LastOrDefault();
				}
				return r;
			}
		}

	}

}
