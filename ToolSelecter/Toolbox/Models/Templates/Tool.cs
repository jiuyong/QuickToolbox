﻿using System;
using System.Collections.Generic;

namespace Jiuyong.Models
{

	public partial class Tool
	{

		#region 数据模型

		public string Description;

		/// <summary>
		/// 显示文本
		/// </summary>
		public string Title;

		public string Name;

		public string FullPath;

		public string Args;

		public DateTime RegisterDateTime;

		public DateTime ModifDateTime;

		/// <summary>
		/// 是否可用
		/// </summary>
		public bool IsEnable = true;

		public string WorkingDirectory;

		public byte[] Icon;

		/// <summary>
		/// 以管理员模式运行
		/// </summary>
		public bool RunAsAdministrator;

		#endregion

	}
}