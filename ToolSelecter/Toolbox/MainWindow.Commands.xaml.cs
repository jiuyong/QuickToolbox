﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Jiuyong.Models;

namespace Jiuyong
{
	/// <summary>
	/// MainWindow.xaml 的交互逻辑
	/// </summary>
	public partial class MainWindow : Window
	{

		partial void AddCommands()
		{
			this.BindCommand(MediaCommands.Play, RunTool);
			this.BindCommand(ApplicationCommands.New, Register);
			this.BindCommand(ApplicationCommands.Delete, Unregister);
			this.BindCommand(ApplicationCommands.Properties, Edit);
			this.BindCommand(MediaCommands.Pause, ChangeEnable);
			this.BindCommand(ApplicationCommands.Save, SaveToolsData);
			this.BindCommand(NavigationCommands.GoToPage, NavigationDirectory);

			//调整顺序
			this.BindCommand(NavigationCommands.FirstPage, MoveToolItemTo);
			this.BindCommand(NavigationCommands.PreviousPage, MoveToolItemTo);
			this.BindCommand(NavigationCommands.NextPage, MoveToolItemTo);
			this.BindCommand(NavigationCommands.LastPage, MoveToolItemTo);
		}

		private void MoveToolItemTo(object sender, ExecutedRoutedEventArgs e)
		{
			var os = e.OriginalSource as Button;
			var dc = os.DataContext as ViewModels.ToolViewModel;
			var drt = (System.Windows.Input.FocusNavigationDirection)e.Parameter;
			VM.EnableTools.MoveTo(drt, dc);
			VM.DisableTools.MoveTo(drt, dc);
		}

		/// <summary>
		/// 打开工作所在目录。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void NavigationDirectory(object sender, ExecutedRoutedEventArgs e)
		{
			var dc = GetMenuCommandModel(e);
			var fullPath = dc.FullPath;
			ExploreFile(fullPath);
		}

		private static void ExploreFile(string fullPath)
		{
			//var dir = System.IO.Path.GetDirectoryName(dc.Shell);
			var args = $"/select, \"{fullPath}\"";
			Process.Start("explorer", args);
		}

		static ViewModels.ToolViewModel GetMenuCommandModel(ExecutedRoutedEventArgs e)
		{
			//var t = e.Parameter as ViewModels.ToolViewModel;//按钮才行，菜单不行。
			var os = e.OriginalSource as FrameworkElement;
			var dc = os.DataContext as ViewModels.ToolViewModel;
			return dc;
		}

		private void SaveToolsData(object sender, ExecutedRoutedEventArgs e)
		{
			VM.ToolsData.Config.Clear();
			try
			{
				foreach (var t in VM.AllTools)
				{
					VM.ToolsData.Config.Add(t.ToModel());
				}
				//VM.ToolsData.ConfigTitle = VM.ToolsData.ConfigSettings.Default.Key;
				VM.ToolsData.SaveSetting(sender, e);
			}
			catch (Exception err)
			{
				Events.Error(err);
			}
		}

		private void ChangeEnable(object sender, ExecutedRoutedEventArgs e)
		{
			dynamic os = e.OriginalSource as Button;
			os.DataContext.IsEnable = !os.DataContext?.IsEnable;
		}

		private void Edit(object sender, ExecutedRoutedEventArgs e)
		{
			var os = e.OriginalSource as Button;
			var dc = os.DataContext as ViewModels.ToolViewModel;

			//v0
			//Core.ShowTextDialog(r =>
			//{
			//	if (String.IsNullOrWhiteSpace(r))
			//	{
			//		//.
			//	}
			//	else
			//	{
			//		dc.Title = r;
			//	}
			//}
			//, dc.Title, (string[])null, messageText: "请输入新的工具标题(_T)：");

			//v1
			var tvm = new ViewModels.ToolViewModel();
			Core.Reflection.MemberwiseCopy(dc, tvm);
			var dlg = new Pages.ToolPage { VM = tvm };
			if (true == dlg.ShowDialog())
			{
				Core.Reflection.MemberwiseCopy(tvm, dc);
			};


			//throw new NotImplementedException();
		}

		private void Unregister(object sender, ExecutedRoutedEventArgs e)
		{
			dynamic os = e.OriginalSource;
			VM.EnableTools.Remove(os.DataContext);
			VM.DisableTools.Remove(os.DataContext);
		}

		private void Register(object sender, ExecutedRoutedEventArgs e)
		{
			var dlg = new Microsoft.Win32.OpenFileDialog()
			{
				DefaultExt = "*.exe",
				//FileName="小工具（可执行程序）",
				Filter = "小工具（可执行程序） (.exe)|*.exe|批处理（.bat）|*.bat|PowerShell脚本（.psl）|*.psl",
				ValidateNames = true
			};

			if (true == dlg.ShowDialog())
			{
				var f = dlg.FileName;
				var t = new Models.Tool();
				t.WorkingDirectory = System.IO.Path.GetDirectoryName(f);
				t.FullPath = f;
				t.Description = f;
				t.Name = System.IO.Path.GetFileNameWithoutExtension(f);
				t.Title = t.Name;
				t.Key = f;
				t.IsEnable = true;
				t.ModifDateTime = DateTime.Now;
				t.RegisterDateTime = DateTime.Now;
				//var vm = new ViewModels.ToolViewModel(t);
				t.Icon = GetIcon(f);
				//VM.Tools.Add(vm);
				VM.AddTool(t);
			};

			//VM.AddTool(sender, e);//Jiuyong:如果逻辑移动出来，如何让其它访问？
		}

		private byte[] GetIcon(string fileName)
		{
			byte[] r = null;

			if (String.IsNullOrWhiteSpace(fileName))
			{
				//.
			}
			else
			{
				var fen = System.IO.Path.GetExtension(fileName.ToLower());
				switch (fen)
				{
					case ".exe":
						r = GetExeIconBinary(fileName);
						break;
					default:
						//.
						break;
				}
			}

			return r;

		}

		public static byte[] GetExeIconBinary(string exeFileName)
		{
			System.Drawing.Icon icon = System.Drawing.Icon.ExtractAssociatedIcon(exeFileName);
			var ms = new MemoryStream();
			var bmp = icon.ToBitmap();
			bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
			ms.Position = 0;
			return ms.ToArray();
		}

		/// <summary>
		/// 打开对应项。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public void RunTool(object sender, ExecutedRoutedEventArgs e)
		{
			var p = e.Parameter as ViewModels.ToolViewModel;

			if (String.IsNullOrWhiteSpace(p?.FullPath))
			{
				//this.AddTool(sender, e);
			}
			else if (p.RunAsAdministrator)
			{
				//多次尝试直接管理员模式运行，但是已有资料尝试均失败。
				//所以这里只能暂时直接定位到程序目录，以便用户手动运行。
				ExploreFile(p.FullPath);
				Events.ShowMessage("程序需要更高权限，请手动运行。");
			}
			else
			{

				var info = new ProcessStartInfo
				{
					Arguments = p.Args,
					CreateNoWindow = false,
					UseShellExecute = false,
					WorkingDirectory = p.WorkingDirectory,
					FileName = p.FullPath,
				};

				//if (p.RunAsAdministrator) info.Verb = "runas";

				var mw = App.Current.MainWindow;
				var _process = new Process();
				_process.StartInfo = info;
				_process.EnableRaisingEvents = true;
				_process.Exited += (p1, p2) =>
				{
					//mw.Dispatcher.BeginInvoke(new Action(()=> { mw.Show();mw.Activate(); }));
					ApplicationSingleton.ActivateOtherInstance();//使用单实例的呈现通知机制。
				};
				try
				{
					if (_process.Start())
					{
						mw.Hide();
					}
				}
				catch (Exception err)
				{
					Events.Error(err);
				}
			}
		}

	}

}
