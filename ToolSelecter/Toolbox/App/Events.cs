﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jiuyong
{
	public class Events : AggregatorEvents
	{
		/// <summary>
		/// 保存配置项。
		/// </summary>
		public static readonly Action SaveConfig
			= EventAggregator.CreateEvent(() => SaveConfig);
	}
}
