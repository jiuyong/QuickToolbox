﻿/// <class>App/</class>
/// <shader>Shaders/</shader>
/// <assembly>Toolbox</assembly>
/// <namespace>Jiuyong</namespace>
///<profile>ps_3_0</profile>
///<discription>实现对前景的描边。</discription>
sampler2D input : register(s0);

/// <summary>描边的颜色</summary>
/// <defaultValue>#FFFFFFFF</defaultValue>
float4 StrokeColor : register(C0);

//使用固定的参数名称以便工具识别。并指定缓冲区位置。
float4 ddxUvDdyUv : register(C1);

float4 main(float2 uv : TEXCOORD) : COLOR
{
	//3x3 纹理坐标采样的偏移以及高斯权重。
    float3 map[8] =
    {
        float3(-1.0, -1.0, 0.8), float3(0, -1.0, 1), float3(1.0, -1.0, 0.8),
        float3(-1.0, 0.0, 1), /* float3(0, -1.0, 1.25),*/float3(1.0, 0.0, 1),
        float3(-1.0, 1.0, 0.8), float3(0, 1.0, 1), float3(1.0, 1.0, 0.8),
    };
    float g = 1;

	////5x5 位置与高斯模糊 但是效果并没有更好。
 //   float g0 = 0.083355;
 //   float g1 = 0.067288;
 //   float g2 = 0.054318;
 //   float g3 = 0.035391;
 //   float g4 = 0.028569;
 //   float g5 = 0.015026;
 //   float g = g2;
 //   float3 map[25] =
 //   {
 //       float3(-2.0, -2.0, g5), float3(-1.0, -2.0, g4), float3(0, -2.0, g3), float3(1.0, -2.0, g4), float3(2.0, -2.0, g5),
 //       float3(-2.0, -1.0, g4), float3(-1.0, -1.0, g2), float3(0, -1.0, g1), float3(1.0, -1.0, g2), float3(2.0, -1.0, g4),
 //       float3(-2.0, 0.0, g3), float3(-1.0, 0.0, g1), float3(0, -1.0, g0), float3(1.0, 0.0, g1), float3(2.0, 0.0, g3),
 //       float3(-2.0, 1.0, g4), float3(-1.0, 1.0, g2), float3(0, 1.0, g1), float3(1.0, 1.0, g2), float3(2.0, 1.0, g4),
 //       float3(-2.0, 2.0, g5), float3(-1.0, 2.0, g4), float3(0, 2.0, g3), float3(1.0, 2.0, g4), float3(2.0, 2.0, g5),
 //   };

    float4 dd = ddxUvDdyUv;
    float4 p0 = tex2D(input, uv);

    float4 b = 0;
    float4 s = StrokeColor;
    //s.rgb = s.rgb * s.a;

	//v1
    for (int i = 0; i < 8; i++)
    {
        float3 mp = map[i];
        float2 p = uv + dd.xy * mp.x + dd.zw * mp.y;
        float4 c = tex2D(input, p);
        c.rgb = c.a * s.rgb * mp.z /* / g*/;
        b = max(b, c);
        //b += c;
    }
	
    //b = b / 25;
	//return b;
	
	//描边
    b.rgb = b.rgb * saturate(1 - p0.a);

    return b + p0;

}

