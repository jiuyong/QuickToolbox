﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Collections.ObjectModel;

namespace Jiuyong
{
	public static partial class ObservableCollectionExtension
	{
		public static void MoveTo<T>(this ObservableCollection<T> my, FocusNavigationDirection direction, T item)
		{
			if (my.Contains(item))
			{
				var idx = my.IndexOf(item);
				switch (direction)
				{
					case FocusNavigationDirection.Next:
						my.Move(idx, Math.Min(idx + 1, my.Count));
						break;
					case FocusNavigationDirection.Previous:
						my.Move(idx, Math.Max(idx - 1, 0));
						break;
					case FocusNavigationDirection.First:
						my.Move(idx, 0);
						break;
					case FocusNavigationDirection.Last:
						my.Move(idx, my.Count - 1);
						break;
					//case FocusNavigationDirection.Left:
					//	break;
					//case FocusNavigationDirection.Right:
					//	break;
					//case FocusNavigationDirection.Up:
					//	break;
					//case FocusNavigationDirection.Down:
					//	break;
					//default:
					//	break;
				}
			}
		}
	}
}
