﻿using Jiuyong.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Jiuyong.Pages
{
	/// <summary>
	/// ToolPage.xaml 的交互逻辑
	/// </summary>
	public partial class ToolPage : Page
	{
		public ToolPage()
		{
			InitializeComponent();
			_viewModel = new ToolViewModel();
			DataContext = _viewModel;
			//Loaded += Page_Loaded;
			this.BindCommand(ApplicationCommands.Save, Save);
			this.BindCommand(ApplicationCommands.Stop, Cancel);
		}

		private void Cancel(object sender, ExecutedRoutedEventArgs e)
		{
			this.CloseDialog();
		}

		private void Save(object sender, ExecutedRoutedEventArgs e)
		{
			this.CloseDialog(true);
		}

		//private void Page_Loaded(object sender, RoutedEventArgs e)
		//{
		//}

		ToolViewModel _viewModel;
		public ToolViewModel VM
		{
			get => _viewModel;
			set
			{
				_viewModel = value;
				DataContext = _viewModel;
			}
		}

	}

	//public partial class ToolPageViewModel : ToolViewModel
	//{

	//}
}
