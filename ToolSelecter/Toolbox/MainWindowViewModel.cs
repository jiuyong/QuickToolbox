﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Jiuyong.Models;

namespace Jiuyong
{

	public class MainWindowViewModel : ViewModelBase
	{

		#region 工具项存档数据

		/// <summary>
		/// 工具项存档数据。
		/// </summary>
		private ViewModels.ConfigViewModel<List<Tool>> _ToolsData
			= new ViewModels.ConfigViewModel<List<Tool>>()
			{
				ConfigFile = "Tools.xml"
			}
			;

		/// <summary>
		/// 工具项存档数据。
		/// </summary>
		//[System.ComponentModel.DataAnnotations.Display(Name="工具项存档数据",Description="工具项存档数据。",AutoGenerateField = true)]
		//[System.ComponentModel.DataAnnotations.Required(ErrorMessage="“工具项存档数据”不能为空！")]
		[System.ComponentModel.Category("模型")]  //可用于属性面板中的分组。
		[System.ComponentModel.Description("工具项存档数据。")]
		////在编辑面板（例如 Blend ）中如何显示。
		[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
		////在属性面板（例如 VisualStudio）中隐藏。
		//[System.ComponentModel.Browsable(false)]
		//[Newtonsoft.Json.JsonIgnore]
		public ViewModels.ConfigViewModel<List<Tool>> ToolsData
		{
			get { return _ToolsData; }
			set
			{
				ValidateProperty(nameof(ToolsData), value);
				_ToolsData = value;
				OnPropertyChanged(nameof(ToolsData));
			}
		}

		#endregion

		/// <summary>
		/// 加载项列表信息。
		/// </summary>
		public void LoadToolsConfig()
		{

			ToolsData.LoadSettings();

			foreach (var t in ToolsData.Config)
			{
				var vm = ViewModels.ToolViewModel.CreateViewModel(t);
				if (vm.IsEnable)
				{
					EnableTools.Add(vm);
				}
				else
				{
					DisableTools.Add(vm);
				}
			}

			//Register("服务器 状态检测", @"AutoPing.exe");
			//var tools = new List<Tool>();
			//tools.Add(new Models.Tool
			//{
			//	Title = "服务器 状态检测",
			//	FullPath = "AutoPing.exe",
			//	Name = System.IO.FullPath.GetFileNameWithoutExtension("AutoPing.exe"),
			//	IsEnable = true
			//});

			//其它非工具按钮都使用右键代替。
			//tools.Add(new Models.Tool
			//{
			//	Title = "添加工具",
			//	Name = "Command:Register",
			//	IsEnable = true
			//});
			//Tools.Add(new Models.Tool
			//{
			//	Title = "保存配置",
			//	Name = "Command:SaveConfig"
			//});
			//for (int i = 0; i < 0; i++)
			//{
			//	//	Tools.Insert(0, new Models.Tool
			//	//	{
			//	//		Title = name,
			//	//		FullPath = path,
			//	//		Name = System.IO.FullPath.GetFileNameWithoutExtension(path)
			//	//	});
			//};

		}

		public void AddTool(Models.Tool tool)
		{
			var vm = ViewModels.ToolViewModel.CreateViewModel(tool);
			EnableTools.Insert(0, vm);
		}

		public ObservableCollection<ViewModels.ToolViewModel> EnableTools { get; }
			= new ObservableCollection<ViewModels.ToolViewModel>();
		public ObservableCollection<ViewModels.ToolViewModel> DisableTools { get; }
			= new ObservableCollection<ViewModels.ToolViewModel>();

		public IEnumerable<ViewModels.ToolViewModel> AllTools
		=> EnableTools.Union(DisableTools);

	}

}
